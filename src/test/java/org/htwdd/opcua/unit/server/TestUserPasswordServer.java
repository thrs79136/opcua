package org.htwdd.opcua.unit.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;

import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.UserPasswordServer;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.UserTokenPolicies;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.core.UserTokenPolicy;
import com.prosysopc.ua.stack.transport.security.SecurityMode;
public class TestUserPasswordServer {

   
	
	@Test
	public void ServerShouldConfigureServerForSpecificUsers() throws UaServerException {
		BasicServer server = mock(BasicServer.class);
		when(server.configure()).thenReturn(new UaServer());		
		UserPasswordServer userPasswordServer = new UserPasswordServer(server);
		UaServer uaServer = userPasswordServer.configure();
		boolean containsPolicy = false;
		for (UserTokenPolicy up : uaServer.getUserTokenPolicies()) {
			if(up == UserTokenPolicies.SECURE_USERNAME_PASSWORD) {
				containsPolicy = true;
			}
		}
		assertEquals(true, containsPolicy);			
	}
	
	@Test
	public void ServerShouldGrantAccessForUser() throws UaServerException, URISyntaxException, InvalidServerEndpointException, ConnectException, ServiceException {
		final String USER_NAME = "alf";
		final String PASSWORD = "melmac";
		BasicServer basic = new BasicServer(5124,"UserPassword");		
		UserPasswordServer upServer = new UserPasswordServer(basic);
		
		UaServer uaServer = upServer.configure();
		uaServer.init();
		uaServer.start();
		String uri = uaServer.getServerUris()[0];
		UaClient client = new UaClient(uri);		
		client.setSecurityMode(SecurityMode.NONE);
		client.setUserIdentity(new UserIdentity(USER_NAME,PASSWORD));
		client.connect();
		
		
	}
}
