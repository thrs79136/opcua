package org.htwdd.opcua.unit.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.server.BasicServer;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class TestBasicServer {
	
	private static final String SERVER_NAME = "STTI";
	private static final int SERVER_PORT = 5000;
	
	/**
	 * Basic Server should create UaServer instance with simples security configuration
	 * @throws UaServerException
	 */
	@Test
	public void ServerShouldBeconfigured() throws UaServerException {
		BasicServer basicServer = new BasicServer(SERVER_PORT, SERVER_NAME);
		UaServer server = basicServer.configure();		
		server.start();
		assertEquals(true, server.isRunning());
		assertEquals(true, server.getSecurityModes().contains(SecurityMode.NONE));
		assertEquals(5000, server.getPort());
		assertEquals(SERVER_NAME, server.getApplicationIdentity().getApplicationDescription().getApplicationName().getText());
		server.close();
	}
}
