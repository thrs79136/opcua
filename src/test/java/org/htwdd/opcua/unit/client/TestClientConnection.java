package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.client.ConnectionSession;
import org.htwdd.opcua.client.DemoClient;
import org.htwdd.opcua.server.BasicServer;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class TestClientConnection {
	
	@Test
	public void ClientShouldConnectToGivenServer() throws UaServerException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException {
				BasicServer s = new BasicServer(55578,"test");
				UaServer server = s.configure();
				server.init();
				server.start();
				DemoClient session = new ConnectionSession(server.getServerUris()[0]);
				
				UaClient client = session.configure();
				client.connect();
				assertEquals(true, client.isConnected());
				assertEquals(SecurityMode.NONE, client.getSecurityMode());
				client.disconnect();
				server.close();
	}
}
