package org.htwdd.opcua.unit.client;

import org.htwdd.opcua.client.ConnectionSession;
import org.htwdd.opcua.client.DemoClient;
import org.htwdd.opcua.client.SubscriptionManagement;
import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.util.AddVariable;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.core.Identifiers;
import com.prosysopc.ua.types.opcua.BaseVariableType;

public class TestSubscriptionValue {

	@Test
	public void ClientShouldReturnReadableValues() throws UaServerException {
		BasicServer s = new BasicServer(55579,"test");
		UaServer server = s.configure();
		server.init();
		server.start();
		AddVariable addVariable = new AddVariable(server.getNodeManagerRoot(),server.getNodeManagerRoot().getObjectsFolder().getNodeId());
		addVariable.addVariable("Test", 1.0, Identifiers.Double);
		
		DemoClient connection = new ConnectionSession(server.getServerUris()[0]);	
		SubscriptionManagement reader = new SubscriptionManagement(connection);
		UaClient client = reader.subscribe("s=Test");
		
		assert client.getSubscriptionCount() == 1;		
		client.disconnect();
	}
}
