package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.htwdd.opcua.client.DemoClient;
import org.htwdd.opcua.client.ReaderDemo;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.Variant;

public class TestReaderDemo {
	
	@Test
	public void ReaderShouldReturnValueFromNode() throws ServiceException, StatusException {
		DemoClient client = mock(DemoClient.class);
		UaClient uaClient = mock(UaClient.class);
		
		NodeId nodeId = NodeId.parseNodeId("ns=1;s=Test");
		DataValue dv = new DataValue();
		dv.setValue(new Variant(20.3));
		
		when(uaClient.readValue(nodeId)).thenReturn(dv);
		when(client.configure()).thenReturn(uaClient);
		
		ReaderDemo reader = new ReaderDemo(client);		
		DataValue value = reader.readValue("ns=1;s=Test");
		assertEquals(20.3, dv.getValue().doubleValue());
		
	}
}
