package org.htwdd.opcua.server;

import java.io.IOException;
import java.io.InputStream;

import org.htwdd.opcua.gen.ServerInformationModel;
import org.xml.sax.SAXException;

import com.prosysopc.ua.ModelException;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;

public class UmweltStationXML {

	private final DemoServer basicServer;
	
	public UmweltStationXML(final DemoServer basicServer) {
		this.basicServer = basicServer;
	}
	
	
	public UaServer ImportXMLModel(String xmlFileName) {
		UaServer server = basicServer.configure();
		
		try {
			server.init();
			InputStream is = server.getClass()
					.getClassLoader().getResourceAsStream(xmlFileName);
			server.registerModel(ServerInformationModel.MODEL);
			server.getAddressSpace().loadModel(is);
		} catch (UaServerException | SAXException | IOException | ModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return server;
		
	}
	
}
