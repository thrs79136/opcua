package org.htwdd.opcua.server;

import java.io.IOException;
import java.net.SocketException;

import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.UaApplication.Protocol;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.transport.security.SecurityMode;
import com.prosysopc.ua.stack.utils.EndpointUtil;

public class BasicServer implements DemoServer {

	private final int port;
	private final String serverName;
	
	public BasicServer(
			final int port,
			final String serverName
			) {
		this.port = port;
		this.serverName = serverName;		
	}
	
	@Override
	public UaServer configure() {
		UaServer server = new UaServer();
		try {
			NoSecurityConfig config = new NoSecurityConfig(
					port,
					serverName,
					EndpointUtil.getInetAddresses(true)
					);
			
			server.setServerName(config.serverName);
			server.setPort(Protocol.OpcTcp,port);
			server.setBindAddresses(config.inetAddress);
			server.getSecurityModes().add(SecurityMode.NONE);
			server.addUserTokenPolicy(config.userTokenPolicy);			
			config.initializeApplicationIdentity(server);
			
		} catch (SecureIdentityException | IOException | UaServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return server;	
	}

}
