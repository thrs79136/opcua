package org.htwdd.opcua.server;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.UserTokenPolicies;
import com.prosysopc.ua.server.ServerUserIdentity;
import com.prosysopc.ua.server.Session;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.server.UserValidator;
import com.prosysopc.ua.stack.core.UserIdentityToken;

public class UserPasswordServer implements DemoServer {

	private final DemoServer basicServer;
	public UserPasswordServer(final DemoServer basicServer){
		this.basicServer = basicServer;
	}
	
	@Override
	public UaServer configure() {
		UaServer server = basicServer.configure();
		try {
			server.removeUserTokenPolicy(UserTokenPolicies.ANONYMOUS);
			server.addUserTokenPolicy(UserTokenPolicies.SECURE_USERNAME_PASSWORD);
			server.setUserValidator(new UPValidator());
		} catch (UaServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return server;	
		
	}
	
	private class UPValidator implements UserValidator{
		private final String USER = "alf";
		private final String PASSWORD = "melmac";
		
		@Override
		public boolean onValidate(Session arg0, ServerUserIdentity arg1) throws StatusException {
			if(arg1.getName().equals(USER)
					&& arg1.getPassword().equals(PASSWORD)) {
				return true;
			}
			return false;
		}

		@Override
		public void onValidationError(Session arg0, UserIdentityToken arg1, Exception arg2) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
}
