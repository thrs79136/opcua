package org.htwdd.opcua.client;

import com.prosysopc.ua.client.UaClient;

public class SubscriptionManagement  {

	DemoClient session;
	public SubscriptionManagement(final DemoClient session){
		this.session = session;
	}
	

	public UaClient subscribe(String nodeId) {	
		UaClient client = session.configure();
		Subscription subscription = new Subscription();
		
		NodeId node = NodeId.parseNodeId(nodeId);
		MonitoredDataItem item = new MonitoredDataItem(node);
		
		MonitoredDataItemListener dataChangeListener = new MonitoredDataItemListener() {
			@Override
			public void onDataChange(MonitoredDataItem sender, DataValue prevValue, DataValue value) {
				System.out.println(sender.toString() + ": " + value.getValue());
			}
		};

		item.setDataChangeListener(dataChangeListener);

		try{
			subscription.addItem(item);
			client.addSubscription(subscription);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return client;
	}
	


}
