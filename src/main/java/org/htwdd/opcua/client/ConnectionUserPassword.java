package org.htwdd.opcua.client;

import java.net.URISyntaxException;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class ConnectionUserPassword implements DemoClient {

	private final String user;
	private final String password;
	private final DemoClient demo;
	
	public ConnectionUserPassword(
			final DemoClient connection,
			final String user,
			final String password
			){
		this.user = user;
		this.password = password;	
		this.demo = connection;
	}
	
	@Override
	public UaClient configure() {
		//return a client with lowest security mode, but user and password identity
		UaClient client = demo.configure();
		client.setSecurityMode(SecurityMode.NONE);
		try{
			client.setUserIdentity(new UserIdentity(user, password));
		} catch (SessionActivationException e) {
			e.printStackTrace();
		}
		return client;
		
	}

}
