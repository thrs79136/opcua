package org.htwdd.opcua.client;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.builtintypes.DataValue;
import com.prosysopc.ua.stack.builtintypes.NodeId;

public class ReaderDemo {

	private final DemoClient demo;

	public ReaderDemo(final DemoClient demo){
		this.demo = demo;
	}
	
	
	public DataValue readValue(String nodeId) {
		//read a simple datavalue by given nodeId from client
		UaClient client = demo.configure();
		try{
			DataValue value = client.readValue(new NodeId(0, nodeId));
			return value;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}

}
