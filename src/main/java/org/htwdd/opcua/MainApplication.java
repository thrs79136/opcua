package org.htwdd.opcua;

import java.io.IOException;
import java.net.URISyntaxException;

import org.htwdd.opcua.gen.Messstation;
import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.UmweltStationXML;
import org.htwdd.opcua.server.util.AddType;
import org.xml.sax.SAXException;

import com.prosysopc.ua.ModelException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.nodes.UaObjectType;
import com.prosysopc.ua.server.InvalidTypeNodeException;
import com.prosysopc.ua.server.NodeBuilderException;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.builtintypes.NodeId;

public class MainApplication {

	
	public static void main(String[] args) throws UaServerException, URISyntaxException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException, SAXException, IOException, ModelException, InvalidTypeNodeException, StatusException, IllegalArgumentException, NodeBuilderException {
		/*BasicServer basicServer = new BasicServer(51234, "HalloWelt");
		UaServer server = basicServer.configure();
		System.out.println(server.getServerUris()[0]);
		server.init();
		server.start();*/
		
		/*BasicServer basicServer = new BasicServer(55555,"test");
		UaServer server = basicServer.configure();
		System.out.println(server.getServerUris()[0]);
		server.init();
		server.start();
		NodeManagerUaNode nodeManager = new NodeManagerUaNode(server, "https://htwdd.de/info");	
		AddType addType = new AddType(nodeManager);
		UaObjectType type = addType.add(server, "MyType");
		NodeId nodeId = type.getNodeId();
		server.getAddressSpace().getNode(nodeId);
		*/
		BasicServer basicServer = new BasicServer(51234, "Umwelt");
		UmweltStationXML uml = new UmweltStationXML(basicServer);
		
		UaServer server = uml.ImportXMLModel("stti.xml");
		server.init();
		server.start();
		System.out.println(server.getServerUris()[0]);
		
		Messstation m = server.getNodeManagerRoot().createInstance(Messstation.class, "Dresden1");
		server.getNodeManagerRoot().getObjectsFolder().addComponent(m);
		m.setTemperatur(20.3);
		
	}
	
	public static void log(Object o) {
		System.out.println(o);
	}
}
