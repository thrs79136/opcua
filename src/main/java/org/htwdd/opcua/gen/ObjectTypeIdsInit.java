// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.stack.builtintypes.ExpandedNodeId;
import com.prosysopc.ua.stack.builtintypes.UnsignedInteger;

class ObjectTypeIdsInit {
  static ExpandedNodeId initMessstation() {
    return new ExpandedNodeId("http://yourorganisation.org/STTI/", UnsignedInteger.valueOf(1002L));
  }
}
